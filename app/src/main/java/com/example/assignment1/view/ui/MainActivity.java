package com.example.assignment1.view.ui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.assignment1.db.DatabaseClient;
import com.example.libmodule.RegisterFragment;
import com.example.myapplication.R;
import com.example.myapplication.db.entity.Employee;
import com.example.registerform.RegistrationFormFragment;

import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity implements RegisterFragment.OnFragmentInteractionListener,
        RegistrationFormFragment.OnFormSubmitClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, new RegisterFragment());
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onFragmentInteraction() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, new RegistrationFormFragment());
        fragmentTransaction.addToBackStack("Register Fragment");
        fragmentTransaction.commit();
    }

    @Override
    public void onFormSubmitButtonClick(JSONObject jsonObject) {
        saveEmpInDb(jsonObject);
    }

    private void saveEmpInDb(final JSONObject jsonObject){

        /*
         * AsyncTask
         */
        class SaveEmpAsync extends AsyncTask<Void, Void, List<Employee>>{

            @Override
            protected List<Employee> doInBackground(Void... voids) {
                Employee emp = new Employee();
                emp.empID = jsonObject.optString("emp_id");
                emp.name = jsonObject.optString("name");
                emp.companyName = jsonObject.optString("company");
                emp.gender = jsonObject.optString("gender");

                DatabaseClient.getInstance(getApplicationContext()).getEmpDatabase()
                        .employeeDao()
                        .insertEmp(emp);

                List<Employee> empList = DatabaseClient.getInstance(getApplicationContext()).getEmpDatabase()
                        .employeeDao()
                        .getEmployeeList();

                return empList;
            }

            @Override
            protected void onPostExecute(List<Employee> empList) {
                Toast.makeText(getApplicationContext(), empList.toString() , Toast.LENGTH_SHORT).show();
                Log.d("========>", empList.toString());
            }
        }

        SaveEmpAsync task = new SaveEmpAsync();
        task.execute();
    }
}
