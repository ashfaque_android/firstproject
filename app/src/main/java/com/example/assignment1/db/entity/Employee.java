package com.example.myapplication.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Employee {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "emp_id")
    public String empID;

    @ColumnInfo(name = "emp_name")
    public String name;

    @ColumnInfo(name = "company_name")
    public String companyName;

    @ColumnInfo(name = "gender")
    public String gender;
}
