package com.example.assignment1.db;

import android.arch.persistence.room.Room;
import android.content.Context;

public class DatabaseClient {

    private static final String DATABASE_NAME = "emp-db";
    private Context mContext;
    private static DatabaseClient sInstance;

    private EmployeeDatabase empDatabase;

    private DatabaseClient(Context context){
        mContext = context;
        empDatabase = Room.databaseBuilder(mContext, EmployeeDatabase.class, DATABASE_NAME).build();
    }

    public static DatabaseClient getInstance(Context context) {
        if(sInstance == null){
            sInstance = new DatabaseClient(context);
        }
        return sInstance;
    }

    public EmployeeDatabase getEmpDatabase(){
        return empDatabase;
    }
}
