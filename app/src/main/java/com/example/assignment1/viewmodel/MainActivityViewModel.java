package com.example.myapplication.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.myapplication.db.entity.Employee;

import java.util.List;

public class MainActivityViewModel extends ViewModel {

    private MutableLiveData<List<Employee>> employee;

    public LiveData<List<Employee>> getEmployeeList(){
        if (employee == null) {
            employee = new MutableLiveData<>();
            loadEmployee();
        }
        return employee;
    }

    private void loadEmployee(){

    }
}
