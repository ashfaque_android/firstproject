package com.example.registerform;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistrationFormFragment extends Fragment {

    private OnFormSubmitClickListener listener;
    private String mGender;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_registration_form, container, false);

    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button submitBtn = (Button) view.findViewById(R.id.btn_submit);
        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_gender);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                mGender = ((RadioButton) (view.findViewById(checkedId))).getText().toString();
            }
        });

        // Submit button click
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("name", ((TextInputEditText) (view.findViewById(R.id.registration_name))).getText());
                    jsonObject.put("company", ((TextInputEditText) (view.findViewById(R.id.registration_company))).getText());
                    jsonObject.put("emp_id", ((TextInputEditText) (view.findViewById(R.id.registration_emp_id))).getText());
                    jsonObject.put("gender", mGender);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                listener.onFormSubmitButtonClick(jsonObject);
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnFormSubmitClickListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /*
     * Interface
     */
    public interface OnFormSubmitClickListener {
        void onFormSubmitButtonClick(JSONObject obj);
    }
}
